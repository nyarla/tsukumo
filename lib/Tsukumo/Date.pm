package Tsukumo::Date;

use strict;
use Any::Moose;
use Any::Moose (
    'X::Types::' . any_moose() => [qw( Str ArrayRef )],
);
use Tsukumo::Types qw( Epoch Year Month Day DayWeek Hour Minute Second TZOffset Date );
use Time::Local qw( timegm );
use Date::Parse;
use namespace::clean -except => [qw( meta )];

my @properties = qw( year month day dayweek hour minute second );

has epoch => (
    is          => 'rw',
    isa         => Epoch,
    required    => 1,
    lazy_build  => 1,
    trigger     => sub {
        for my $property ( @properties ) {
            my $method = "clear_${property}";
            $_[0]->$method();
        }

        $_[0]->__clear_time;
        $_[0]->clear_gmt;
    }
);

sub _build_epoch {
    my ( $self ) = @_;

    if ( ! grep { exists $self->{$_} } @properties ) {
        return time;
    }

    my ( $year, $month, $day, $hour, $minute, $second )
        = @{ $self }{qw( year month day hour minute second )};

    $year   ||= [gmtime]->[5];
    $month  ||= 1;
    $day    ||= 1;
    $hour   ||= 0;
    $minute ||= 0;
    $second ||= 0;

    return timegm( $second, $minute, $hour, $day, ( $month -1 ), $year );
}

has __time => (
    is          => 'ro',
    isa         => ArrayRef[Str],
    lazy_build  => 1,
    clearer     => '__clear_time',
    builder     => '__build_time',
);

sub __build_time {[ gmtime $_[0]->epoch ]}

has [ @properties ] => (
    is          => 'rw',
    lazy_build  => 1,
    trigger     => sub {
        $_[0]->clear_epoch;
        $_[0]->__clear_time;
        $_[0]->clear_gmt;
    }
);

has '+year'     => ( isa => Year );
has '+month'    => ( isa => Month );
has '+day'      => ( isa => Day );
has '+dayweek'  => ( isa => DayWeek );
has '+hour'     => ( isa => Hour );
has '+minute'   => ( isa => Minute );
has '+second'   => ( isa => Second );

sub _build_year     { $_[0]->__time->[5] + 1900 }
sub _build_month    { $_[0]->__time->[4] + 1    }
sub _build_day      { $_[0]->__time->[3]        }
sub _build_dayweek  { $_[0]->__time->[6]        }
sub _build_hour     { $_[0]->__time->[2]        }
sub _build_minute   { $_[0]->__time->[1]        }
sub _build_second   { $_[0]->__time->[0]        }

has tzoffset => (
    is          => 'rw',
    isa         => TZOffset,
    coerce      => 1,
    lazy_build  => 1,
);

sub _build_tzoffset {
    my $now = time;
    return timegm(localtime($now)) - timegm(gmtime($now));
}

has gmt => (
    is          => 'rw',
    isa         => Date,
    init_arg    => undef,
    lazy_build  => 1,
);

sub _build_gmt {
    my ( $self ) = @_;
    my $class = ref $self;

    return $class->new(
        epoch       => $self->epoch + $self->tzoffset,
        tzoffset    => 0,
    );
}

sub now {
    my ( $class, @args ) = @_;
    return $class->new( @args, epoch => time );
}

sub parse_dwim {
    my ( $class, $date ) = @_;
    my ( $sec, $min, $hr, $da, $mo, $yr, $zone ) = Date::Parse::strptime($date);

    return $class->new(
        year        => $yr + 1900,
        month       => $mo + 1,
        day         => $da,
        hour        => $hr,
        minute      => $min,
        second      => $sec,
        tzoffset    => $zone,
    );
}

sub clone {
    my ( $self, %args ) = @_;

    for my $prop (qw( year month day hour minute second tzoffset )) {
        $args{$prop} = $self->$prop()
            if ( ! exists $args{$prop} );
    }

    return ref($self)->new( %args );
}

sub ymd {
    my ( $self, $sep ) = @_;
    $sep = q{} if ( ! defined $sep );

    my $year    = sprintf( '%04d', $self->year  );
    my $month   = sprintf( '%02d', $self->month );
    my $day     = sprintf( '%02d', $self->day   );

    return join $sep, ( $year, $month, $day );
}

sub time {
    my ( $self, $sep ) = @_;
    $sep = q{} if ( ! defined $sep );

    my $hour    = sprintf( '%02d', $self->hour  );
    my $minute  = sprintf( '%02d', $self->minute);
    my $second  = sprintf( '%02d', $self->second);

    return join $sep, ( $hour, $minute, $second );
}

sub w3cdtf {
    my ( $self ) = @_;

    my $date    = $self->ymd('-');
    my $time    = $self->time(':');
    my $offset  = $self->tzoffset;

    my $tz;
    if ( $offset == 0 ) {
        $tz = 'Z';
    }
    else {
        my $pm  = ( $offset < 0 ) ? '-' : '+';
        my $hr  = int( $offset / ( 60 * 60 ) );
        my $min = ( $offset - ( $hr * 60 * 60 ) ) / 60;
           $tz  = $pm . join(q{:}, sprintf('%02d', $hr), sprintf('%02d', $min));
    }

    return "${date}T${time}${tz}";
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Tsukumo::Date - Datetime object for Tsukumo.

=head1 SYNPOSIS

    use Tsukumo::Date;
    
    my $now     = Tsukumo::Date->now;
    my $date    = Tsukumo::Date->new( epoch => $time );

=head DESCRIPTION

This class is datetime object for Tsukumo.

=head1 PROPERTIES

=head2 year

This property folds year.

=head2 month

This property folds month,

This property value is 1 - 12.

=head2 day

This property folds day.

This property value is 1 - 31.

=head2 hour

This property is folds hour.

This property value is 0 - 23.

=head2 minute

This proeprty is folds minute.

This property value is 0 - 59.

=head2 second

This propety is folds second.

This property value is 0 - 60.

=head2 tzoffset

This propety folds timezone offset.

You can set offset epoch or timezone string (C<Z>, C<+{hr}:{min}> or C<-{hr}:{min}> )

=head2 gmt

This property returns datetime object at gmt.

=head1 METHODS

=head2 new

    my $date = Tsukumo::Date->new( epoch => time );

This method is constructor of Tsukumo::Date.

=head2 now

    my $now = Tsukumo::Date->now;

This method makes instance from time now.

=head2 parse_dwim

    my $date = Tsukumo::Date->parse_dwim( $date );

This method parses some datetime string.

This method is implemented by L<Date::Parse>,
so please see L<Parse::Date> document about supported formats.

=head2 clone

    my $dateA = Tsukumo::Date->now;
    my $dateB = $dateA->clone

This method clones objecy.

=head2 ymd

    my $str = $date->ymd('-');  # YYYY-MM-DD
    my $str = $date->ymd('/');  # YYYY/MM/DD

=head2 time

    my $str = $date->time(':'); # HH:MM:SS
    my $str = $date->time('.'); # HH.MM.SS

=head2 w3cdtf

    my $w3cdtf = $date->w3cdtf;

This method returns W3C datetime format.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ASLO

L<Date::Parse>, L<Time::Local>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.


=cut
