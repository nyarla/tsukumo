package Tsukumo::Types;

use strict;
use Any::Moose;
use Any::Moose (
    'X::Types'                  => [ -declare => [qw(
        Variables API Cache LogFormatter Logger
        Config Context
        Epoch Year Month Day DayWeek Hour Minute Second TimeZone TZOffset Date
    )] ],
    'X::Types::' . any_moose()  => [qw( Undef Str Int Object ArrayRef HashRef CodeRef )],
);

use FindBin ();
use Tsukumo::Exceptions qw(
    not_supported parameter_missing
    file_not_found file_io_error
    evaluate_error invalid_type_error
);
use Time::Piece;

my @roles   = qw(
    Tsukumo::Cache
    Tsukumo::Logger
);

for my $role ( @roles ) {
    role_type($role) if (! find_type_constraint($role) );
}

my @classes = qw(
    Tsukumo::Variables
    Tsukumo::API
    Tsukumo::Date
    Tsukumo::Config
    Tsukumo::Context
);

for my $class ( @classes ) {
    class_type($class) if ( ! find_type_constraint($class) );
}

# Variables
subtype Variables,
    as 'Tsukumo::Variables',
;

coerce Variables,
    from HashRef,
        via { Tsukumo::Variables->new($_) },
;

# API
subtype API,
    as 'Tsukumo::API',
;

# Cache
our %CacheInterface = (
    'Cache'                             => 'Cache',
    'Cache::Cache'                      => 'CacheCache',
    'CHI'                               => 'CHI',
    'Cache::Memcached'                  => 'Memecached',
    'Cache::Memcached::Fast'            => 'Memcached',
    'Cache::Memcached::libmemcached'    => 'Memcached',
    'Cache::FastMmap'                   => 'FastMmap',
);

subtype Cache,
    as 'Tsukumo::Cache',
;

coerce Cache,
    from Undef,
        via {
            require Tsukumo::Cache::Memory;
            Tsukumo::Cache::Memory->new();
        },
    from HashRef,
        via {
            my %config = %{ $_ };
            
            my $class = delete $config{'class'}
                or parameter_missing error => "HashRef -> Cache: Cache class is not specified.";
            my $args  = delete $config{'args'} || {};
            my $deref = !! $config{'deref'};

            Any::Moose::load_class($class)
                if ( ! Any::Moose::is_class_loaded($class) );

            my $klass;
            for my $isa ( keys %CacheInterface ) {
                if ( $class eq $isa || $class->isa($isa) ) {
                    $klass = $isa;
                }
            }

            not_supported error => "HashRef -> Cache: ${class} is not supported."
                if ( ! $klass );

            my $interface = $CacheInterface{$klass};
               $interface = "Tsukumo::Cache::${interface}";

            Any::Moose::load_class($interface)
                if ( ! Any::Moose::is_class_loaded($interface) );

            my $cache = $class->new( ( $deref ) ? %{ $args } : $args );

            $interface->new( cache => $cache );
        },
    from Object,
        via {
            my $cache = $_;
            my $class;

            for my $isa ( keys %CacheInterface ) {
                if ( ref $cache eq $isa || $cache->isa($isa) ) {
                    $class = $isa;
                }
            }

            not_supported "Object -> Cache: " . ref($cache) . "is not supported."
                if ( ! $class );

            my $interface = $CacheInterface{$class};
               $interface = "Tsukumo::Cache::${interface}";

            Any::Moose::load_class($interface)
                if ( ! Any::Moose::is_class_loaded($interface) );

            $interface->new( cache => $cache );
        },
;

# LogFormatter

subtype LogFormatter,
    as CodeRef,
;

coerce LogFormatter,
    from Undef,
        via {
            sub {
                my ( %p ) = @_;
                my $l = delete $p{'level'};
                my $m = delete $p{'message'};
                
                return "[${l}] ${m}";
            }
        },
    from Str,
        via {
            my $format      = $_;
            my $need_caller = $format =~ m/[{]package|line|file[}]/;
            sub {
                my ( %p ) = @_;

                if ( $need_caller ) {
                    my $depth = 0;
                       $depth++ while ( caller($depth) =~ m{^Log::Dispatch} );
                       $depth += $Tsukumo::Logger::CallerDepth;
                    @p{qw( package file line )}
                        = caller($depth);
                }

                my $log = $format;
                   $log =~ s/[{]date[(](.+?)[)][}]/
                        my $fmt = $1;
                        localtime->strftime($fmt);
                   /egx;
                   $log =~ s/[{](.+?)[}]/
                        my $tag = $1;
                        if ( $tag eq 'date' ) {
                            scalar localtime;
                        }
                        elsif ( exists $p{$tag} ) {
                            $p{$tag};
                        }
                        else {
                            q{};
                        }
                   /egx;

                return $log;
            }
        },
;

# Logger
subtype Logger,
    as 'Tsukumo::Logger',
;

coerce Logger,
    from Undef,
        via {
            Any::Moose::load_class('Tsukumo::Logger::Null')
                if ( ! Any::Moose::is_class_loaded('Tsukumo::Logger::Null') );
            Tsukumo::Logger::Null->new();
        },
    from HashRef,
        via {
            my %config = %{ $_ };

            my $logger  = delete $config{'logger'}
                or parameter_missing error => "HashRef -> Logger: logger class is not specified.";
               $logger  = "Tsukumo::Logger::${logger}";
            my %args    = %config;

            Any::Moose::load_class($logger)
                if ( ! Any::Moose::is_class_loaded($logger) );

            $logger->new( %args );
        },
;

# Config
subtype Config,
    as 'Tsukumo::Config'
;

coerce Config,
    from Undef,
        via {
            my $env     = uc('tsukumo_config');
            my $file    = ( exists $ENV{$env} ) ? $ENV{$env} : "${FindBin::Bin}/config.pl";

            file_not_found  error => "Undef -> Config: File does not exists: ${file}"  if ( ! -e $file );
            file_io_error   error => "Undef -> Config: Cannot read file: ${file}"      if ( ! -r $file );

            my $config = do $file;

            evaluate_error      error => "Undef -> Config: Failed to load configuation file: ${file}: ${@}" if ( $@ );
            invalid_type_error  error => "Undef -> Config: Configuration data is not HASH reference."
                if ( ref $config ne 'HASH' );

            Tsukumo::Config->new( $config );
        },
    from Str,
        via {
            my $file    = $_;

            file_not_found  error => "Str -> Config: File does not exists: ${file}"   if ( ! -e $file );
            file_io_error   error => "Str -> Config: Cannot read file: ${file}"       if ( ! -r $file );

            my $config  = do $file;

            evaluate_error      error => "Str -> Config: Failed to load configuration file: ${file}: ${@}" if ( $@ );
            invalid_type_error  error => "Str -> Config: Configuration data is not HASH reference."
                if ( ref $config ne 'HASH' );
        
            Tsukumo::Config->new( $config );
        },
    from ArrayRef,
        via {
            my $config = Tsukumo::Config->new;
            for my $file ( @{ $_ } ) {
                file_not_found  error => "ArrayRef -> Config: File does not exists: ${file}"   if ( ! -e $file );
                file_io_error   error => "ArrayRef -> Config: Cannot read file: ${file}"       if ( ! -r $file );
                
                my $data = do $file;
                
                evaluate_error      error => "ArrayRef -> Config: Failed to load configuration file: ${file}: ${@}" if ( $@ );
                invalid_type_error  error => "AraryRef -> Config: Configuration data is not HASH reference."
                    if ( ref $data ne 'HASH' );
                
                $config->merge( $data );
            }

            $config;
        },
    from HashRef,
        via {
            Tsukumo::Config->new($_);
        },
    from CodeRef,
        via {
            my $config = $_->();

            invalid_type_error erro => "CodeRef -> Config: Configuration data is not HASH reference."
                if ( ref $config ne 'HASH' );

            Tsukumo::Config->new( $config );
        },
;

# Context

subtype Context,
    as 'Tsukumo::Context',
;

coerce Context,
    from ArrayRef,
        via { Tsukumo::Context->new( @{ $_ } ) },
    from HashRef,
        via { Tsukumo::Context->new( %{ $_ } ) },
;

# Epoch
subtype Epoch,
    as Int,
    where { $_ >= 0 },
    message { "${_} is not epoch" }
;

# Year
subtype Year,
    as Int,
    where   { $_ >= 1970 },
    message { "${_} is not after 1970." }
;

# Month
subtype Month,
    as Int,
    where   { 1 <= $_ && $_ <= 12 },
    message { "${_} is not 1 - 12" }
;

# Day
subtype Day,
    as Int,
    where   { 1 <= $_ && $_ <= 31 },
    message { "${_} is not 1 - 31" },
;

# DayWeek
subtype DayWeek,
    as Int,
    where   { 1 <= $_ && $_ <= 7 },
    message { "${_} is not 1 - 7" },
;

# Hour
subtype Hour,
    as Int,
    where   { 0 <= $_ && $_ <= 23 },
    message { "${_} is not 0 - 23" },
;

# Minute
subtype Minute,
    as Int,
    where   { 0 <= $_ && $_ <= 59 },
    message { "${_} is not 0 - 59" },
;

# Second
subtype Second,
    as Int,
    where   { 0 <= $_ && $_ <= 60 },
    message { "${_} is not 0 - 60" },
;

# TimeZone
subtype TimeZone,
    as Str,
    where   { $_ eq 'Z' || $_ =~ m{^[\-+]\d{2}:\d{2}$} },
    message { "Invalid timezone format: ${_}" },
;

# TZOffset
subtype TZOffset,
    as Int,
;

coerce TZOffset,
    from TimeZone,
        via {
            return 0 if ( $_ eq 'Z' );
    
            my ( $pm, $hour, $minute )
                = ( $_ =~ m{^([\-+])(\d{2}):(\d{2})$} );
            my $time = ( $hour * 60 * 60 ) + ( $minute * 60 );
               $time = $time * -1 if ( $pm eq '-' );

            return $time;
        },
;

# Date
subtype Date,
    as 'Tsukumo::Date',
;

coerce Date,
    from Undef,
        via { Tsukumo::Date->now },
    from Epoch,
        via { Tsukumo::Date->new( epoch => $_ ) },
    from Str,
        via { Tsukumo::Date->parse_dwim($_) },
    from ArrayRef,
        via { Tsukumo::Date->new( @{ $_ } ) },
    from HashRef,
        via { Tsukumo::Date->new( %{ $_ } ) },
;

for my $role ( @roles ) {
    Any::Moose::load_class($role)
        if ( ! Any::Moose::is_class_loaded($role) );
}

for my $class ( @classes ) {
    Any::Moose::load_class($class)
        if ( ! Any::Moose::is_class_loaded($class) );
}

__PACKAGE__->meta->make_immutable;
__END__

=head1 NAME

Tsukumo::Types - (Moose|Mouse) types for Tsukumo.

=head1 SYNPOSIS

    package MyApp::Class;
    
    use Any::Moose;
    use Tsukumo::Types qw( Config );
    
    has config => (
        is          => 'rw',
        isa         => Config,
        coerce      => 1,
        required    => 1,
    );

=head1 DESCRIPTION

This types are (Moose|Mouse) types for Tsukumo.

=head1 TYPES

=head2 C<Variables> - type of L<Tsukumo::Varialbes>

coerce:

=over

=item HashRef

When HashRef passed as an argument,
the argument converts Tsukumo::Variables instance.

=back

=head2 C<API> - type of L<Tsukumo::API>

=head2 C<Cache> - role type of L<Tsukumo::Cache>

coerce:

=over

=item Undef

When Undef was passed as an argument,
this coerce rule returns L<Tsukumo::Cache::Memory> object.

=item HashRef

When HashRef was passed as an argument,
this coerce rule makes Cache class instance from HashRef parameter.

HashRef parameters:

=over

=item class

Cache class name is designated.

This parameter is required.

=item args

Arguments to the onstructor in cache class is designated.

=item deref

When this arguments is true, C<args> is dereferenced.

=back

=item Object

When Object was passed as an arguments,
this coerce rule converts instance to Cache class object.

=back

=head2 C<LogFormatter> type of log message formatter

coerce:

=over

=item Undef

When Undef was passed as an argument,
this coerce rule returns default formatter.

=item Str

When Str was passed as an argument,
this coerce rule compile format string.

The format tag which can be used for a log message is as follows:

=over

=item C<level>

log message level

=item C<message>

log message

=item C<package>

package name where logger was called

=item C<file>

filename where logger was called

=item C<line>

line of file where logger was called

=item C<date>

datetime when logger was called

=item C<date($format)>

formatted datetime when logger was called.

Please see L<Time::Piece> document about how to format datetime.

=back

=back

=head2 C<Logger> - role type of L<Tsukumo::Logger>

coerce:

=over

=item Undef

When Undef was passed as an argument,
this coerce rule returns L<Tsukumo::Logger::Null>'s instance.

=item HashRef

When HashRef was passwd as an argument,
this coerce rule makes Logger class instance from HashRef parameter.

HashRef parameter:

=over

=item logger

It's designated which logger to use.

A prefix as C<Tsukumo::Logger> sticks to this argument automatically.

=item all other arguments

All other arguments are passed to a constructor in the class designated in argument C<logger>.

=back

=back

=head2 C<Config> - type of L<Tsukumo::Config>

coerce:

=over

=item Undef

When Undef was passed as an arguments,
this coerce rule reads perl script file from C<$ENV{'TSUKUMO_CONFIG'}> or C<${FindBin::Bin}/config.pl>.

=item Str

When Str was passed as an argument,
this coerce rule reads perl script file from an argument.

=item ArrayRef

When ArrayRef was passed as an argument,
this coerce rule reads some script file from an arguments.

And a designated files is load as a configuration file.

=item HashRef

When HashRef was passed as an argument,
the argument passed Tsukumo::Config's constructor.

=item CodeRef

When CodeRef was passed as an argument,
The result which executed an argument is passed to Tsukumo::Config's constructor.

=back

=head2 C<Context> - type of L<Tsukumo::Context>

coerce:

=over

=item ArrayRef

When ArrayRef was passed as an argument,
a argument passed Tsukumo::Context's constructor.

=item HashRef

When HashRef was passed as an argument,
a argument passed Tsukumo::Context's constructor.

=back

=head2 C<Epoch>, C<Year>, C<Month>, C<Day>, C<DayWeek>, C<Hour>, C<Minute>, C<Second>, C<TimeZome> and C<TZOffset>

These properties are datetime properties.

=head2 C<Date> type of L<Tsukumo::Date>

This type is type of L<Tsukumo::Date>.

coerce:

=over

=item Undef

When Undef was passed as this rule,
this rule calls L<Tsukumo::Date-E<gt>now>.

=item Epoch

When Epoch was passed as this rule,
this rule calls L<Tsukumo::Date-E<gt>new( epoch =E<gt> $argument )>.

=item Str

When Str was passed as this rule,
this rule calls L<Tsukumo::Date-E<gt>parse_dwim($argument)>.

=item ArrayRef, HashRef

When ArrayRef or HashRef was passed as this rule,
arguments are passed L<Tsukumo::Date> constructor.

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
