package Tsukumo::Logger;

use strict;
use Any::Moose '::Role';

our $CallerDepth = 0;

requires qw( log );

sub debug   { $_[0]->log( level => 'debug',     message => $_[1] ) }
sub info    { $_[0]->log( level => 'info',      message => $_[1] ) }
sub notice  { $_[0]->log( level => 'notice',    message => $_[1] ) }
sub warn    { $_[0]->log( level => 'warn',      message => $_[1] ) }
sub error   { $_[0]->log( level => 'error',     message => $_[1] ) }

1;
__END__

=head1 NAME

Tsukumo::Logger - Logger interface definition for Tsukumo.

=head1 SYNPOSIS

    package MyLogger;
    
    use strict;
    use Any::Moose;
    
    with 'Tsukumo::Logger';
    
    sub log {
        my ( $self, %p ) = @_;
        my $level   = delete $p{'level'};
        my $message = delete $p{'message'};

        # your logging code here.
    }
    
    package main;
    
    my $logger = MyLogger->new();
       $logger->debug($message);

=head1 DESCRIPTION

This class is (Moose|Mouse) role for Logger interface.

=head1 INTERFACE METHODS

=head2 new

    my $logger = MyLogger->new;

This method is constructor.

=head2 log

    $logger->log( level => 'debug', message => $message );

This method logs a message.

Arguments:

=over

=item C<level>

Log message level is specified by this argument.

This argument is required.

=item C<message>

Log message is specified by this argument.

This argument is required.

=back

=head2 debug

    $logger->debug($message);

This method is alias of C<$logger-E<gt>log( level => 'debug', message =E<gt> $message )>.

=head2 info

    $logger->info($message);

This method is alias of C<$logger-E<gt>log( level => 'info', message =E<gt> $message )>.


=head2 notice

    $logger->notice($message);

This method is alias of C<$logger-E<gt>log( level => 'notice', message =E<gt> $message )>.

=head2 warn

    $logger->warn($message);

This method is alias of C<$logger-E<gt>log( level => 'warn', message =E<gt> $message )>.

=head2 error

    $logger->error($message);

This method is alias of C<$logger-E<gt>log( level => 'error', message =E<gt> $message )>.


=head1 REQUIREMENT METHODS

=head1 log

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thoetp.netE<gt>

=head1 SEE ALSO

L<Tsukumo::Logger::LogDispatch>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
