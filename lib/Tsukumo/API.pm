package Tsukumo::API;

use strict;
use Any::Moose;
use Tsukumo::Exceptions qw( invalid_argument_error );
use namespace::clean -except => [qw( meta )];

has hooks => (
    is      => 'rw',
    isa     => 'HashRef[ArrayRef]',
    default => sub { +{} },
);

sub register_hook {
    my ( $self, $instance, @actions ) = @_;
    my $hooks = $self->hooks;

    while ( my ( $hook, $action ) = splice @actions, 0, 2 )  {
        invalid_argument_error error => "Hook action is not CODE referernce"
            if ( ref $action ne 'CODE' );

        $hooks->{$hook} ||= [];
        push @{ $hooks->{$hook} }, +{
            instance    => $instance,
            action      => $action,
        };
    }

    return $self;
}

sub run_hooks {
    my ( $self, $name, $args, %option ) = @_;
    my $callback = delete $option{'callback'};
    my $runonce  = delete $option{'runonce'} || 0;
       $args     = [ $args ] if ( ref $args ne 'ARRAY' );

    my @ret;
    if ( ! exists $self->hooks->{$name} ) {
        return ();
    }

    for my $hook ( @{ $self->hooks->{$name} } ) {
        my ( $instance, $action )
            = @{ $hook }{qw( instance action )};

        my $ret = $action->( $instance, @{ $args } );

        $callback->( $name, $ret, $instance, @{ $args } )
            if ( ref $callback eq 'CODE' );

        return $ret if ( defined $ret && !! $runonce );

        push @ret, $ret;
    }

    return @ret;
}

sub run_hook_once {
    my ( $self, $hook, $args, %option ) = @_;
    $option{'runonce'} = 1;
    return $self->run_hooks( $hook, $args, %option );
}

1;

=head1 NAME

Tsukumo::API - API folder for Tsukumo.

=head1 SYNPOSIS

    use Tsukumo::API;
    
    my $api = Tsukumo::API->new;
       $api->register_hook(
            $plugin,
            'hook.name' => $plugin->can('action'),
       );
       $api->run_hook('hook.name' => [ @args ]);

=head1 DESCRIPTION

This class is API folder object for Tsukumo.

=head1 METHODS

=head2 new

    my $api = Tsukumo::API->new;

This method is constructor of Tsukumo::API

=head2 register_hook

    $api->register_hook(
        $instance,
        'hook.name' => $instance->can('action')
    );

This method registers instance and hook actions.

Arguments passes an instance or a class first 
and next designates hook actions by the form as C<'hook.name =E<gt> $instance-E<gt>can('action')>.

=head2 run_hooks

    $api->run_hook( 'hook.name' => [ @args ], %options );

This method runs all hook actions specified by argument.

The designation of arguments are as follows:

=over

=item C<'hook.name'>

A hook name is designated by this argument.

This argment is required.

=item C<[ @args ]>

Arguments to a hook action is designated by this argument.

This argument is required.

=item C<%options>

The following option can be designated as this argument:

=over

=item C<\&callback>

    sub {
        my ( $hookname, $result, $instance, @args ) = @_;
        # your code here
    }

This option can designate callback to a result of the hook action.

The argument of callback is as follows:

=over

=item C<$hookname>

Hook action name.

=item <$result>

Results of hook action.

=item C<$instance>

Instance or class of hook action.

=item C<@args>

Arguments of hook action.

=back

=item C<$runonce>

when this option is truth and a hook action returns the effective value,
This method finishes other hook actions.

=back

These options are optional

=back

=head2 run_hook_once

    my $ret = $hook->run_hook_once( 'hook.name' => [ @args ], %option ).

This method is alias of C<$api->run_hooks( 'hook.name' =E<gt> [ @args ], %options, runonce =E<gt> 1 )>.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

=cut
