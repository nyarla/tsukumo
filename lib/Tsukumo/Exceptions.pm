package Tsukumo::Exceptions;

use strict;
use warnings;

use parent qw( Exporter );

my ( %E, @E );
our ( @EXPORT_OK, %EXPORT_TAGS );
BEGIN {

    %E = (
        'Tsukumo::Exception'                            => {
            description => 'Basical exception for Tsukumo.',
            alias       => 'basic_error',
        },
    
        'Tsukumo::Exception::InvalidTypeError'          => {
            isa         => 'Tsukumo::Exception',
            description => 'Type validation error',
            alias       => 'invalid_type_error',
        },
        'Tsukumo::Exception::InvalidArgumentError'      => {
            isa         => 'Tsukumo::Exception::InvalidTypeError',
            description => 'Argument is not valid.',
            alias       => 'invalid_argument_error',
        },
    
        'Tsukumo::Exception::ParameterMissing'          => {
            isa         => 'Tsukumo::Exception',
            description => 'Required parameter is missing',
            alias       => 'parameter_missing',
        },
        'Tsukumo::Exception::ArgumentParameterMissing'  => {
            isa         => 'Tsukumo::Exception::ParameterMissing',
            description => 'Requires argument parameter is missing',
            alias       => 'argument_parameter_missing',
        },
    
        'Tsukumo::Exception::NotSupported'              => {
            isa         => 'Tsukumo::Exception',
            description => 'Tsukumo is not supporting.',
            alias       => 'not_supported',
        },
    
        'Tsukumo::Exception::FileNotFound'              => {
            isa         => 'Tsukumo::Exception',
            description => 'Fille not found',
            alias       => 'file_not_found',
        },
    
        'Tsukumo::Exception::IOError'                   => {
            isa         => 'Tsukumo::Exception',
            description => 'Basical IO Error',
            alias       => 'io_error',
        },
        'Tsukumo::Exception::FileIOError'               => {
            isa         => 'Tsukumo::Exception::IOError',
            description => 'File IO Error',
            alias       => 'file_io_error',
        },
    
        'Tsukumo::Exception::EvaluateError'             => {
            isa         => 'Tsukumo::Exception',
            description => 'Evaluate code error',
            alias       => 'evaluate_error',
        },
    
    );

    @E = keys %E;

    for my $define ( values %E ) {
        if ( exists $define->{'alias'} ) {
            push @EXPORT_OK, $define->{'alias'};
        }
    }

    %EXPORT_TAGS = ( all => [ @EXPORT_OK ] );
}

use Exception::Class ( %E );

$_->Trace(1) for @E;

1;

=head1 NAME

Tsukumo::Exceptions - Exception classes for Tsukumo.

=head1 SYNPOSIS

    use Tsukumo::Exceptions qw( basic_error );
    
    basic_error error => $error_message;

=head1 DESCRIPTION

This module is exception classes container for Tsukumo.

=head1 EXCEPTIONS

=head2 basic_error - C<Tsukumo::Exception>

=head2 invalid_type_error - C<Tsukumo::Exception::InvalidTypeError>

=head2 invalid_argument_error - C<Tsukumo::Exception::InvalidArgumentError>

=head2 parameter_missing - C<Tsukumo::Exception::ParameterMissing>

=head2 argument_parameter_missing - C<Tsukumo::Exception::ArgumentParameterMissing>

=head2 not_supported - C<Tsukumo::Exception::NotSupported>

=head2 file_not_found - C<Tsukumo::Exception::FileNotFound>

=head2 io_error - C<Tsukumo::Exception::IOError>

=head2 file_io_error - C<Tsukumo::Exception::FileIOError>

=head2 evaluate_error - C<Tsukumo::Exception::EvaluateError>

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
