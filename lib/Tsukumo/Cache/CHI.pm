package Tsukumo::Cache::CHI;

use strict;
use Any::Moose;
use namespace::clean -except => [qw( meta )];

with 'Tsukumo::Cache';

has '+cache' => (
    isa => 'CHI',
);

sub set {
    my ( $self, $key, $value ) = @_;
    $self->cache->set( $key, $value );
}

sub get {
    my ( $self, $key ) = @_;
    $self->cache->get( $key );
}

sub remove {
    my ( $self, $key ) = @_;
    $self->cache->remove( $key );
}

sub clear {
    my ( $self ) = @_;
    $self->cache->clear();
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Tsukumo::Cache::CHI - L<CHI> adapter

=head1 SYNPOSIS

    my $cache = Tskumo::Cache::CHI->new(
        cache => CHI->new( %args ),
    );
    
    $cache->set( $key, $value );
    my $val = $cache->get( $key );

=head1 DESCRIPTION

This class is L<CHI> adapter for Tsukumo.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<CHI>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
