package Tsukumo::Cache::Memcached;

use strict;
use Any::Moose;
use Any::Moose '::Util::TypeConstraints' => [qw( duck_type )];
use namespace::clean -except => [qw( meta )];

with 'Tsukumo::Cache';

has '+cache' => (
    isa => duck_type([qw( set set_multi get get_multi delete delete_multi flush_all )]),
);

sub set {
    my ( $self, $key, $value ) = @_;
    $self->cache->set( $key, $value );
}

sub set_multi {
    goto $_[0]->cache->can('set_multi');
}

sub get {
    my ( $self, $key ) = @_;
    $self->cache->get($key);
}

sub get_multi {
    goto $_[0]->cache->can('get_multi');
}

sub remove {
    my ( $self, $key ) = @_;
    $self->cache->delete( $key );
}
sub remove_multi {
    goto $_[0]->cache->can('delete_multi');
}

sub clear {
    my ( $self ) = @_;
    $self->cache->flush_all;
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Tsukumo::Cache::Memcached - Memcached classes adapter

=head1 SYNPOSIS

    my $cache = Tskumo::Cache::Memcached->new(
        cache => Cache::Memcahced::Fast->new( %args );
    );
    
    $cache->set( $key, $value );
    my $val = $cache->get( $key );

=head1 DESCRIPTION

This class is L<Cache::Memcached> like classes adapter for Tsukumo.

=head1 REQUIREMENT INTERFACE

The cache class where it's passed to this adapter requests the following interface:

=over

=item set

=item set_multi

=item get

=item get_multi

=item delete

=item delete_multi

=item flush_all

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Cache::Memcached>, L<Cache::Memcached::Fast>, L<Cache::Memcached::libmemcached>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
