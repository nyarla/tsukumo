package Tsukumo::Cache::Memory;

use strict;
use Any::Moose;
use namespace::clean -except => [qw( meta )];

with 'Tsukumo::Cache';

has '+cache' => (
    isa     => 'HashRef',
    default => sub { +{} },
);

sub set {
    my ( $self, $key, $value ) = @_;
    $self->cache->{$key} = $value;
    return 1;
}

sub get {
    my ( $self, $key ) = @_;
    return $self->cache->{$key};
}

sub remove {
    my ( $self, $key ) = @_;
    return ( delete $self->cache->{$key} ) ? 1 : undef ;
}

sub clear {
    my ( $self ) = @_;
    $self->cache({});
    return 1;
}

__PACKAGE__->meta->make_immutable;

=head1 NAME

Tsukumo::Cache::Memory - Memory Cache

=head1 SYNPOSIS

    my $cache = Tskumo::Cache::Memory->new;
    
    $cache->set( $key, $value );
    my $val = $cache->get( $key );

=head1 DESCRIPTION

This class is memory cache class for Tsukumo.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
