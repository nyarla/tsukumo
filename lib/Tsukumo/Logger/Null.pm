package Tsukumo::Logger::Null;

use strict;
use Any::Moose;
use namespace::clean -excpet => [qw( meta )];

with 'Tsukumo::Logger';

sub log { 1 }

__PACKAGE__->meta->make_immutable;

=head1 NAME

Tsukumo::Logger::Null - Null logger for Tsukumo.

=head1 SYNPOSIS

    use Tsukumo::Logger::Null;
    
    my $logger = Tsukumo::Logger::Null->new;
       $logger->debug('message');

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
