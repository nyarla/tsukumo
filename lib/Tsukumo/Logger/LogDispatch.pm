package Tsukumo::Logger::LogDispatch;

use strict;

use Any::Moose;
use Tsukumo::Types qw( LogFormatter );
use Tsukumo::Exceptions qw( parameter_missing );
use Scalar::Util qw( blessed );
use namespace::clean -except => [qw( meta )];

use Log::Dispatch;

with 'Tsukumo::Logger';

has dispatchers => (
    is      => 'ro',
    isa     => 'ArrayRef[Str]',
    default => sub { +[qw( null )] },
);

has loggers => (
    is      => 'rw',
    isa     => 'HashRef[HashRef|Log::Dispatch::Output]',
    default => sub { +{ null => { class => 'Log::Dispatch::Null', min_level => 'error' } } },
);

has format => (
    is          => 'rw',
    isa         => LogFormatter,
    coerce      => 1,
    lazy_build  => 1,
);

sub _build_format {
    return sub {
        my ( %p ) = @_;
        my $l = delete $p{'level'};
        my $m = delete $p{'message'};

        return "[${l}] ${m}";
    };
}

has logger => (
    is          => 'rw',
    isa         => 'Log::Dispatch',
    lazy_build  => 1,
    init_arg    => undef,
);

sub _build_logger {
    my ( $self ) = @_;
    my $log = Log::Dispatch->new;

    for my $name ( @{ $self->dispatchers } ) {
        my $config = $self->loggers->{$name};

        parameter_missing error => "config.log.${name} is not defined."
            if ( ! defined $config );

        if ( blessed($config) ) {
            $log->add( $config );
        }
        else {
            my %config = %{ $config };
            my $class  = delete $config{'class'}
                or parameter_missing error => "config.log.${name}.class is required.";
            my %args   = %config;

            Any::Moose::load_class($class)
                if ( Any::Moose::is_class_loaded($class) );

            $log->add( $class->new( name => $name, %args ) );
        }
    }

    $log->add_callback( $self->format );

    return $log;
}

sub log {
    my ( $self, %p ) = @_;
    my $caller = caller(0);

    my $level   = delete $p{'level'};
    my $message = delete $p{'message'};

    my $depth   = 1;
       $depth++ if ( $caller eq 'Tsukumo::Logger' );
    local $Tsukumo::Logger::CallerDepth = $depth;

    $self->logger->log( level => $level, message => $message );
}

around BUILDARGS => sub {
    my ( $orig, $class, %args ) = @_;

    my %param;
       $param{'dispatchers'}    = delete $args{'dispatchers'};
       $param{'format'}         = delete $args{'format'};
       $param{'loggers'}        = { %args };

    return $class->$orig( %param );
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Tsukumo::Logger::LogDispatch - Log::Dispatch adaptor for Tsukumo.

=head1 SYNPOSIS

    use Tsukumo::Logger::LogDispatch;
    
    my $logger = Tsukumo::Logger::LogDispatch->new(
        dispatchers => [qw( file screen )],
        format      => q'[{date}] [{package}] {message} at {file} line {line}',
        file        => {
            class => 'Log::Dispatch::File',
            %fileargs,
        },
        screen      => {
            class => 'Log::Dispatch::Screen',
            %screenargs,
        },
    );
    
    $logger->debug('message!');

=head1 DESCRIPTION

This logger is adaptor of L<Log::Dispatcher>.

=head1 CONSTRUCTOR

    my $logger = Tsukumo::Logger::LogDispatch->new(
        format      => q"[{level} {message}]" || sub { my ( %p ) = @_; },
        dispatchers => [qw( screen file )],
        screen      => {
            class       => 'Log::Dispatch::Screen',
            min_level   => 'debug',
        },
        file        => Log::Dispatch::File->new( %args ),
    );

This method is constructor of C<Tsukumo::Logger::LogDispatch>.

B<Arguments>:

=over

=item C<format>

The format of the log message is designated.

The log format is designated by the form as C<{name}>.

The variable which can be designated as C<{name}> see L<Tsukumo::Types> C<LogFormatter> section.

=item C<dispatchers>

The name of logger is designated.

This argument is required.

=item Argument of the same name as logger designated as argument 'dispatcher'

Details of logger are designated by this argument.

The value which can be designated as this argument:

=over

=item instance of sub-class in L<Log::Dispatch::Output>

An instance in a sub-class in L<Log::Dispatch::Output> is designated.

=item configuration hash reference of sub-class in L<Log::Dispatch::Output>

Hash reference including several setting is designated.

Hash reference values:

=over

=item C<class>

A sub-class in L<Log::Dispatch::Output> is designated.

examples:

    Log::Dispatch::File
    Log::Dispatch::Screen
    ...etc

=item The argument which can be designated in a sub-class in L<Log::Dispatch::Output>.

The argument which can be designated in a sub-class in Log::Dispatch::Output can be designated.

The value of argument dispatcher which designates hash reference including this setting is used
for the value of argument name.

=back

=back

=back

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
