package Tsukumo::Config;

use strict;
use warnings;

use parent qw( Tsukumo::Variables );

1;
__END__

=head1 NAME

Tsukumo::Config - Configuration data object for Tsukumo.

=head1 SYNPOSIS

    use Tsukumo::Config;
    
    my $config = Tsukumo::Config->new;
    my $config = Tsukumo::Config->new( \%config );
    
    $config->{'foo'}->{'bar'};

=head1 DESCRIPTION

This class is configuration hash object class for Tsukumo.

=head1 METHODS

=head2 new

    my $config = Tsukumo::Config->new;
    my $config = Tsukumo::Config->new( \%config );

This method is constructor of Tsukumo::Config.

Only HASH reference can be passed to an argument.

=head2 merge

    $config->merge( \%hashA, \%hashB, ... \%hashN );

This method merges hash reference.

This method implemetned by L<Hash::Merge::Simple>,
so B<this method does not check for cycles>.

=head1 visit

    $config->visit(sub {
        my $value = shift;
        # do something
        return $value;
    });

This method is config tree visitor.

CODE reference is designated as an argument.

The value of config tree is passed to callback,
and callback has to return the new value.

=head1 decode/encode

    # decode config tree
    $config->decode('utf8');

    # encode config tree
    $config->encode('utf8');

This method decodes/encode config tree.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
