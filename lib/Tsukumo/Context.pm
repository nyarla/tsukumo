package Tsukumo::Context;

use strict;
use Any::Moose;
use Tsukumo::Types qw( Config Variables API Cache Logger );
use namespace::clean -except => [qw( meta )];

has config => (
    is          => 'rw',
    isa         => Config,
    coerce      => 1,
    required    => 1,
);

has stash => (
    is          => 'rw',
    isa         => Variables,
    coerce      => 1,
    lazy_build  => 1,
);

sub _build_stash {
    my ( $self ) = @_;
    return Tsukumo::Variables->new( $self->config->{'stash'} || {} );
}

has api => (
    is          => 'rw',
    isa         => API,
    lazy_build  => 1,
    trigger     => sub {
        my ( $self, $new ) = @_;
        $new->context( $self );
    },
);

sub _build_api {
    my ( $self ) = @_;

    my $api = Tsukumo::API->new;
       $api->context( $self );

    return $api;
}

has cache => (
    is          => 'rw',
    isa         => Cache,
    lazy_build  => 1,
    coerce      => 1,
);

sub _build_cache {
    my ( $self ) = @_;

    if ( exists $self->config->{'cache'} ) {
        return $self->config->{'cache'};
    }

    return undef;
}

has log => (
    is          => 'rw',
    isa         => Logger,
    lazy_build  => 1,
    coerce      => 1,
);

sub _build_log {
    my ( $self ) = @_;

    if ( exists $self->config->{'log'} ) {
        return $self->config->{'log'};
    }

    return undef;
}

# has templates => ();
# has entries => ();
# has server => ();
# has controllers => ();

__PACKAGE__->meta->make_immutable;

=head1 NAME

Tsukumo::Conetxt - Context class for Tsukumo.

=head1 SYNPOSIS

    my $context = Tsukumo::Context->new( %args );
    
    $context->log->debug('debug message');

=head1 DESCRIPTION

This class is context class for Tsukumo.

=head1 PROPERTIES

=head2 config

This property is accessor to configuration hash.

Configuration hash is L<Tsukumo::Config> object.

This property is required by deafult.

=head2 stash

This property is accessor of stash hash.

Stash has is L<Tsukumo::Variables> object.

=head2 api

This property is accessor of API holder object.

API holder object is L<Tsukumo::API> object.

=head2 cache

This property is accessor of cache object.

Cache object is instance of L<Tsukumo::Cache> family.

=head2 log

This property is accessor of logger object.

Logger object is instance of L<Tsukumo::Logger> family.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

