package Tsukumo::Cache;

use strict;
use Any::Moose '::Role';
use namespace::clean -except => [qw( meta )];

requires qw(
    get set remove clear
    get_multi set_multi remove_multi
);

has cache => (
    is          => 'rw',
    isa         => 'Object',
    required    => 1,
);

sub set_multi {
    my ( $self, @list ) = @_;
    my @ret;
    my %ret;
    for my $set ( @list ) {
        my $ret = $self->set( $set->[0], $set->[1] );
        push @ret, $ret;
        $ret{$set->[0]} = $ret;
    }

    return @ret if ( wantarray );
    return { %ret };
}

sub get_multi {
    my ( $self, @key ) = @_;
    my %data = ();

    for my $key ( @key ) {
        my $value = $self->get($key);
        $data{$key} = $value if ( defined $value );
    }

    return { %data };
}

sub remove_multi {
    my ( $self, @key ) = @_;

    my @ret = ();
    my %ret = ();

    for my $key ( @key ) {
        my $ret = $self->remove($key);
        push @ret, $ret;
        $ret{$key} = $ret;
    }

    return @ret if ( wantarray );
    return { %ret };
}

1;

=head1 NAME

Tsukumo::Cache - Cache interface definition for Tsukumo.

=head1 SYNPOSIS

    package MyCacheClass;
    
    use strict;
    use Any::Moose;
    
    with 'Tsukumo::Cache';
    
    sub set     { # set code here       }
    sub get     { # get code here       }
    sub remove  { # rremove code here   } 
    sub clear   { # clear code here     }

=head1 DESCRIPTION

This class is a (Moose|Mouse) role for Cache interface.

=head1 INTERFACE METHODS

=head2 new

    my $cache = Tsukumo::Cache::AdapterClass->new(
        cache => $RowCacheInstance,
    );

This method is constructor.

An instance in a cache class is passed to argument L<cache>.
This argument is indispensable.

=head2 set

    $cache->set( $key, $value );

C<$value> is set under the name as C<$key> in cache.

=head2 set_multi

    $cache->set_multi(
        [ $keyA, $valA ],
        [ $keyB, $valB ],
        ...
    );

Several values are set at the same time in cache.

=head2 get

    my $value = $self->get($key);

The value of C<$key> gets out from cache.

=head2 get_multi

    my $values = $self->get_multi( @keys );
    
    $values->{$keyA};
    $values->{$keyB};

Get several values from cache,

A return value is the hash reference and each value is stocked by key name of cache.

=head2 remove

    $cache->remove($key);

C<$key> is removes from cache.

=head2 remove_multi

    $cache->remove_multi( @keys );

several values are removed from cache.

=head2 clear

    $cache->clear;

clear all cache.

=head1 REQUIREMENT METHODS

=head2 set

=head2 get

=head2 remove

=head2 clear

=head1 OPTIONAL REQUIREMENT METHODS

=head2 set_multi

=head2 get_multi

=head2 remove_mutli

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 SEE ALSO

L<Tsukumo::Cache::Cache>

L<Tsukumo::Cache::CacheCache>

L<Tsukumo::Cache::CHI>

L<Tsukumo::Cache::Memcached>

L<Tsukumo::Cache::FastMmap>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
