package Tsukumo::Variables;

use strict;
use warnings;

use Tsukumo::Exceptions;
use Hash::Merge::Simple;
use Data::Rmap ();
use Encode ();

sub new {
    my $class   = shift;
    my $vars  = shift || {};

    Tsukumo::Exception::InvalidArgumentError->throw( error => "Variables data is not HASH reference" )
        if ( ! ref $vars || ref $vars ne 'HASH' );

    my $self = bless $vars, $class;

    return $self;
}

sub merge {
    my ( $self, @args ) = @_;

    %{ $self } = %{ Hash::Merge::Simple->merge( $self, @args ) };

    return $self;
}

sub visit {
    my $self        = shift;
    my $callback    = shift or Tsukumo::Exception::ArgumentParameterMissing->throw( error => "Callback is required." );
    Tsukumo::Exception::InvalidArgumentError->throw( error => "Callback is not CODE reference" )
        if ( ref $callback ne 'CODE' );

    Data::Rmap::rmap { $_ = $callback->($_) } $self;

    return $self;
}

sub decode {
    my $self        = shift;
    my $encoding    = shift or Tsukumo::Exception::ArgumentParameterMissing->throw( error => "Decode encoding is required." );

    my $enc = Encode::find_encoding($encoding);
    $self->visit(sub { return $enc->decode($_[0]) });

    return $self;
}

sub encode {
    my $self        = shift;
    my $encoding    = shift or Tsukumo::Exception::ArgumentParameterMissing->throw( error => "Encode encoding is required." );

    my $enc = Encode::find_encoding($encoding);
    $self->visit(sub { return $enc->encode($_[0]) });

    return $self;
}

1;
__END__

=head1 NAME

Tsukumo::Variables - Variables data object for Tsukumo.

=head1 SYNPOSIS

    use Tsukumo::Variables;
    
    my $vars = Tsukumo::Varibales->new;
    my $vars = Tsukumo::VAriables->new( \%vars );
    
    $vars->{'foo'}->{'bar'};

=head1 DESCRIPTION

This class is variable hash object class for Tsukumo.

=head1 METHODS

=head2 new

    my $vars = Tsukumo::Variables->new;
    my $vars = Tsukumo::Variables->new( \%config );

This method is constructor of Tsukumo::Variables.

Only HASH reference can be passed to an argument.

=head2 merge

    $vars->merge( \%hashA, \%hashB, ... \%hashN );

This method merges hash reference.

This method implemetned by L<Hash::Merge::Simple>,
so B<this method does not check for cycles>.

=head1 visit

    $vars->visit(sub {
        my $value = shift;
        # do something
        return $value;
    });

This method is config tree visitor.

CODE reference is designated as an argument.

The value of config tree is passed to callback,
and callback has to return the new value.

=head1 decode/encode

    # decode config tree
    $vars->decode('utf8');

    # encode config tree
    $vars->encode('utf8');

This method decodes/encode config tree.

=head1 AUTHOR

Naoki Okamura (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
