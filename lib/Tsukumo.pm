package Tsukumo;

use strict;
use Any::Moose '::Role';
use Tsukumo::Types qw( Context );
use namespace::clean -except => [qw( meta )];

our $VERSION = '0.00001';

requires qw( setup );

has context => (
    is          => 'rw',
    isa         => Context,
    lazy_build  => 1,
    builder     => 'setup',
);

sub to_app {
    my ( $proto ) = @_;

    my $app     = ( ref $proto ) ? $proto : $proto->new ;
    my $context = $app->context;

    return sub {
        $context->server->parse_request(shift);
        $context->dispatcher->disaptch;
        return $context->server->response->finalize;
    };
}


1;
__END__

=head1 NAME

Tsukumo - Pluggable and configurable contents management system

=head1 SYNOPSIS

    --- MyApp.pm
    package MyApp;
    
    use strict;
    use Any::Moose;
    use namespace::clean -excpet => [qw( meta )];
    
    with 'Tsukumo';
    
    sub setup {
        my ( $self ) = @_;
        # MyApp setup here
    }
    
    --- app.psgi
    #!/usr/bin/perl
    
    use strict;
    use warnings;
    
    use lib '/path/to/myapp/lib/';
    use MyApp;
    
    return MyApp->to_app;

=head1 DESCRIPTION

Tsukumo is pluggable and configurable contents management system.

=head1 AUTHOR

Naoki Okamrua (Nyarla) E<lt>nyarla[ at ]thotep.netE<gt>

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
