#!perl

use strict;
use warnings;

use Test::More;

BEGIN {
    use_ok('Tsukumo::Logger::Null');
}

my $logger = Tsukumo::Logger::Null->new;

isa_ok( $logger, 'Tsukumo::Logger::Null' );

ok( $logger->debug('message!') );

done_testing;
