#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Exceptions qw(:all);
my @tests = (
    {
        function    => 'basic_error',
        isa         => 'Tsukumo::Exception',
    },

    {
        function    => 'invalid_type_error',
        isa         => 'Tsukumo::Exception::InvalidTypeError',
    },
    {
        function    => 'invalid_argument_error',
        isa         => 'Tsukumo::Exception::InvalidArgumentError',
    },

    {
        function    => 'parameter_missing',
        isa         => 'Tsukumo::Exception::ParameterMissing',
    },
    {
        function    => 'argument_parameter_missing',
        isa         => 'Tsukumo::Exception::ArgumentParameterMissing',
    },

    {
        function    => 'not_supported',
        isa         => 'Tsukumo::Exception::NotSupported',
    },

    {
        function    => 'file_not_found',
        isa         => 'Tsukumo::Exception::FileNotFound',
    },

    {
        function    => 'io_error',
        isa         => 'Tsukumo::Exception::IOError',
    },
    {
        function    => 'file_io_error',
        isa         => 'Tsukumo::Exception::FileIOError',
    },

    {
        function    => 'evaluate_error',
        isa         => 'Tsukumo::Exception::EvaluateError',
    },
);

for my $test ( @tests ) {
    my ( $function, $isa ) = @{ $test }{qw( function isa )};

    is( $isa->Trace, 1 );

    local $@;
    eval {
        no strict 'refs';
        &{$function}( error => 'test exception!' );
    };

    isa_ok( $@, $isa );
    is( $@->message, 'test exception!' );
}

done_testing;
