#!perl

use strict;
use warnings;

use Tsukumo::Types qw( Context );
use Test::More;

my $meta = Context;

is( "${meta}", 'Tsukumo::Types::Context' );

ok( $meta->check( Tsukumo::Context->new( config => {} ) ) );
ok( ! $meta->check({}) );

is_deeply(
    $meta->coerce([ config => { foo => 'bar' } ]),
    $meta->coerce({ config => { foo => 'bar' } }),
);

done_testing;
