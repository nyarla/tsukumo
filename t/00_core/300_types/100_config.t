#!perl

use strict;
use warnings;

use Test::More;
use t::Util qw( $examples );
use Tsukumo::Types qw( Config );

my $meta = Config;

is( "${meta}", 'Tsukumo::Types::Config' );

ok( $meta->check(Tsukumo::Config->new) );
ok( ! $meta->check({}) );

local $ENV{'TSUKUMO_CONFIG'} = "${examples}/core.types.config/bar.pl";

is_deeply(
    $meta->coerce(undef),
    Tsukumo::Config->new({ bar => 'BBB' }),
);

is_deeply(
    $meta->coerce("${examples}/core.types.config/foo.pl"),
    Tsukumo::Config->new({ foo => 'AAA' }),
);


is_deeply(
    $meta->coerce([ "${examples}/core.types.config/foo.pl", "${examples}/core.types.config/bar.pl" ]),
    Tsukumo::Config->new({ foo => 'AAA', bar => 'BBB' }),
);

is_deeply(
    $meta->coerce({ foo => 'bar' }),
    Tsukumo::Config->new({ foo => 'bar' }),
);

is_deeply(
    $meta->coerce(sub { return { baz => 'foo' } }),
    Tsukumo::Config->new({ baz => 'foo' }),
);

done_testing;
