#!perl

use strict;
use warnings;

use Tsukumo::Types qw( Cache );
use Tsukumo::Cache::Memory;
use Tsukumo::Cache::CacheCache;
use Test::More;

{
    package Cache::Cache;
    
    sub new { bless {}, shift }
    
    package Cache::MemoryCache;
    
    use parent qw( Cache::Cache );
}

my $meta = Cache;

is( "${meta}", 'Tsukumo::Types::Cache' );

ok( $meta->check( Tsukumo::Cache::Memory->new ) );
ok( ! $meta->check({}) );

is_deeply(
    $meta->coerce(undef),
    Tsukumo::Cache::Memory->new,
);

is_deeply(
    $meta->coerce({ class => 'Cache::MemoryCache', args => {}, deref => 0 }),
    Tsukumo::Cache::CacheCache->new( cache => Cache::MemoryCache->new ),
);

is_deeply(
    $meta->coerce( Cache::MemoryCache->new ),
    Tsukumo::Cache::CacheCache->new( cache => Cache::MemoryCache->new ),
);

done_testing;
