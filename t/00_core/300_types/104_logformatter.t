#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Types qw( LogFormatter );

my $meta = LogFormatter;

is( "${meta}", 'Tsukumo::Types::LogFormatter' );

ok( $meta->check(sub {}) );
ok( ! $meta->check({}) );

my @tests = (
    {
        format  => undef,
        level   => 'debug',
        message => 'this is debug message',
        match   => qr{^\[debug\] this is debug message$},
    },
    {
        format  => q"{level}|{message}",
        level   => 'info',
        message => 'information!',
        match   => qr{^info[|]information!$},
    },
    {
        format  => q"{package}|{file}|{line}|{date}",
        level   => 'info',
        message => 'foobar',
        match   => qr{^main[|]t/00_core/300_types/104_logformatter.t[|]\d+[|].+?$},
    },
    {
        format  => q"{date(%Y-%m-%d)}",
        level   => 'info',
        message => 'foobar',
        match   => qr{^\d{4}-\d{2}-\d{2}},
    },
    {
        format  => sub { return 'foo' },
        level   => 'error',
        message => 'foobar',
        match   => qr{^foo$},
    }
);

for my $config ( @tests ) {
    my ( $format, $level, $message, $match )
        = @{ $config }{qw( format level message match )};
    my $formatter = $meta->coerce( $format );

    like(
        $formatter->( level => $level, message => $message ),
        $match,
    );
}

done_testing;
