#!perl

use strict;
use warnings;

use Tsukumo::Types qw( API );
use Test::More;

my $meta = API;

is( "${meta}", 'Tsukumo::Types::API' );

ok( $meta->check( Tsukumo::API->new() ) );
ok( ! $meta->check({}) );

done_testing;
