#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Types qw( Date );

my $meta = Date;

is( "${meta}", 'Tsukumo::Types::Date' );

ok( $meta->check( Tsukumo::Date->now ) );
ok( ! $meta->check(time) );

isa_ok( $meta->coerce(undef), 'Tsukumo::Date' );

is_deeply(
    $meta->coerce('2010-04-10T10:20:30+09:00'),
    Tsukumo::Date->parse_dwim('2010-04-10T10:20:30+09:00'),
);

my $time = time;

is_deeply(
    $meta->coerce($time),
    Tsukumo::Date->new( epoch => $time ),
);

is_deeply(
    $meta->coerce([ epoch => $time ]),
    $meta->coerce({ epoch => $time }),
);

done_testing;
