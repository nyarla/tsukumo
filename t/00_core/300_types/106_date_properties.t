#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Types qw( Epoch Year Month Day DayWeek Hour Minute Second TimeZone TZOffset );

my $epoch = Epoch;

is( "${epoch}", 'Tsukumo::Types::Epoch' );

ok( $epoch->check( 1 ), );
ok( ! $epoch->check( -1 ) );


my $year = Year;

is( "${year}", 'Tsukumo::Types::Year' );

ok( $year->check(2009) );
ok( ! $year->check(10) );

my $month = Month;

is( "${month}", 'Tsukumo::Types::Month' );

ok( $month->check(12) );
ok( ! $month->check(34) );

my $day = Day;

is( "${day}", 'Tsukumo::Types::Day' );

ok( $day->check(31) );
ok( ! $day->check(0) );

my $dayweek = DayWeek;

is( "${dayweek}", 'Tsukumo::Types::DayWeek' );

ok( $dayweek->check(2) );
ok( ! $dayweek->check(8) );

my $hour = Hour;

is( "${hour}", 'Tsukumo::Types::Hour' );

ok( $hour->check(10) );
ok( ! $hour->check(40) );

my $minute = Minute;

is( "${minute}", 'Tsukumo::Types::Minute' );

ok( $minute->check(40) );
ok( ! $minute->check(90) );

my $second = Second;

is( "${second}", 'Tsukumo::Types::Second' );

ok( $second->check(60) );
ok( ! $second->check(-1) );

my $timezone = TimeZone;

is( "${timezone}", 'Tsukumo::Types::TimeZone' );

ok( $timezone->check('Z') );
ok( $timezone->check('-09:00') );
ok( $timezone->check('+09:00') );
ok( ! $timezone->check('foo') );

my $tzoffset = TZOffset;

is( "${tzoffset}", 'Tsukumo::Types::TZOffset' );

ok( $tzoffset->check(1000) );
ok( $tzoffset->check(-1000) );
ok( ! $tzoffset->check('a039') );

is( $tzoffset->coerce('Z'), 0 );
is( $tzoffset->coerce('+09:00'), 32400 );
is( $tzoffset->coerce('-09:00'), -32400 );

done_testing;

