#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Types qw( Variables );

my $meta = Variables;

is( "${meta}", 'Tsukumo::Types::Variables' );

ok( $meta->check(Tsukumo::Variables->new) );
ok( ! $meta->check({}) );

is_deeply(
    $meta->coerce({ foo => 'bar' }),
    Tsukumo::Variables->new({ foo => 'bar' }),
);

done_testing;
