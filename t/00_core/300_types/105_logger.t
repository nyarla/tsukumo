#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Logger::Null;
use Tsukumo::Types qw( Logger );

my $meta = Logger;

is( "${meta}", 'Tsukumo::Types::Logger' );

ok( $meta->check( Tsukumo::Logger::Null->new ) );
ok( ! $meta->check({}) );

is_deeply(
    $meta->coerce(undef),
    Tsukumo::Logger::Null->new,
);

is_deeply(
    $meta->coerce({ logger => 'Null' }),
    Tsukumo::Logger::Null->new,
);

done_testing;
