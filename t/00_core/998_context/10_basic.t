#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Context;

# -- config -------------------------- #

my $config = {
    foo => 'bar',
};

my $context = Tsukumo::Context->new( config => $config );

is_deeply(
    $context->config,
    $config,
);

# -- stash --------------------------- #

# build
$context->config->{'stash'} = { var => 'IYH' };

is_deeply(
    $context->stash,
    { var => 'IYH' },
);

$context->clear_stash;

$context->stash({ foo => 'bar' });

is_deeply(
    $context->stash,
    { foo => 'bar' },
);

$context->clear_stash;

# -- api ----------------------------- #

my $api = $context->api;

is( $api->context, $context );

$context->clear_api;

# -- cache --------------------------- #

my $cache = $context->cache;

isa_ok( $cache, 'Tsukumo::Cache::Memory' );

$context->clear_cache;

{
    package Cache::Test;
    our @ISA = qw( Cache );
    sub new { bless {}, shift };
}

$context->config->merge({
    cache => {
        class   => 'Cache::Test',
        args    => {},
        deref   => 1,
    },
});

$cache = $context->cache;

isa_ok( $cache, 'Tsukumo::Cache::Cache' );

$context->clear_cache;
$context->config->{'cache'} = {};

# -- logger -------------------------- #

my $logger = $context->log;

isa_ok( $logger, 'Tsukumo::Logger::Null' );

$context->clear_log;

$context->config->merge({
    log => {
        logger => 'Null',
    },
});

$logger = $context->log;

isa_ok( $logger, 'Tsukumo::Logger::Null' );

$context->clear_log;
$context->config->{'log'} = {};

# ------------------------------------ #

done_testing;
