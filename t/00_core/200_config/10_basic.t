#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Config;

isa_ok('Tsukumo::Config', 'Tsukumo::Variables');

done_testing;
