#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Date;
use Time::Local qw( timegm );

my $time    = 0;
my $offset  = timegm(localtime(0)) - timegm(gmtime(0));
   $offset  = 0 if ( $offset < 0 );
my $date    = Tsukumo::Date->new( epoch => $time );

is( $date->epoch, 0 );
is( $date->year, 1970 );
is( $date->month, 1 );
is( $date->day, 1 );
is( $date->hour, 0 );
is( $date->minute, 0 );
is( $date->second, 0 );
is( $date->tzoffset, $offset );
is( $date->gmt->epoch, $time + $offset );
is( $date->gmt->tzoffset, 0 );

$date->year(2010);
$date->month(4);
$date->day(10);
$date->hour(5);
$date->minute(15);
$date->second(25);

is(
    $date->epoch,
    timegm( 25, 15, 5, 10, ( 4 - 1 ), ( 2010 - 1900 ) )
);

done_testing;
