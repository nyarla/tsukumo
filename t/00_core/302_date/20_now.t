#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Date;

isa_ok( Tsukumo::Date->now, 'Tsukumo::Date' );

done_testing;
