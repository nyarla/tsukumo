#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Date;

my $date = Tsukumo::Date->new( hour => 10, minute => 20, second => 30 );

is( $date->time, '102030' );
is( $date->time(':'), '10:20:30' );

done_testing;
