#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Date;

my $w3cdtf  = '2010-04-10T10:20:30+09:00';
my $date    = Tsukumo::Date->parse_dwim($w3cdtf);

is( $date->w3cdtf, $w3cdtf );

done_testing;
