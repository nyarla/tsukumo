#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Date;

my $date = Tsukumo::Date->parse_dwim('2010-04-01T10:20:30+09:00');

is( $date->year, 2010 );
is( $date->month, 04 );
is( $date->day, '01' );
is( $date->hour, 10 );
is( $date->minute, 20 );
is( $date->second, 30 );
is( $date->tzoffset, 9 * 60 * 60 );

done_testing;
