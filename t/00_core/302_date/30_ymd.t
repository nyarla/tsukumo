#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Date;

my $date = Tsukumo::Date->new( year => 2010, month => 4, day => 10 );

is( $date->ymd, '20100410' );

is( $date->ymd('/'), '2010/04/10' );

done_testing;
