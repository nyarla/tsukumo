#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Date;

my $date = Tsukumo::Date->new( epoch => time );
my $clone = $date->clone;

for my $prop (qw( year month day hour minute second tzoffset )) {
    is( $date->$prop(), $clone->$prop() );
}

$clone = $date->clone( year => 2020 );

for my $prop (qw( month day hour minute second tzoffset )) {
    is( $date->$prop(), $clone->$prop() );
}

is( $clone->year, 2020 );

done_testing;
