#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Variables;

my $config = Tsukumo::Variables->new({ foo => 'foo', bar => 'bar' });

$config->visit(sub { uc $_[0] });

is_deeply(
    $config,
    {
        foo => 'FOO',
        bar => 'BAR',
    },
);

done_testing;
