#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Variables;

my $config = Tsukumo::Variables->new({ foo => 'あいう' });

$config->decode('utf8');

ok( utf8::is_utf8($config->{'foo'}) );

done_testing;
