#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Variables;

my $config = Tsukumo::Variables->new({ foo => 'bar', baz => { foo => 'AAA' } });

$config->merge(
    { bar => [qw( foo bar )] },
    { baz => { bar => 'BBB' } },
);

is_deeply(
    $config,
    {
        foo => 'bar',
        bar => [qw( foo bar )],
        baz => {
            foo => 'AAA',
            bar => 'BBB',
        },
    },
);

done_testing;
