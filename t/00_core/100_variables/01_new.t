#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Variables;

my $config = Tsukumo::Variables->new;

isa_ok( $config, 'Tsukumo::Variables' );

$config = Tsukumo::Variables->new({ foo => 'bar' });

isa_ok( $config, 'Tsukumo::Variables' );

is_deeply(
    $config,
    { foo => 'bar' },
);

done_testing;