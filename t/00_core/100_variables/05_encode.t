#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Variables;

my $config = Tsukumo::Variables->new({
    foo => Encode::decode('utf8', 'あいう'),
});

ok( utf8::is_utf8($config->{'foo'}) );

$config->encode('utf8');

ok( ! utf8::is_utf8($config->{'foo'}) );

done_testing;
