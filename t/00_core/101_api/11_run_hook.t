#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::API;

{
    package MyPluginA;
    
    sub new { bless {}, shift }
    
    sub foo {
        my ( $self, @args ) = @_;
        
        main::isa_ok( $self, 'MyPluginA' );
        main::is_deeply(
            [ @args ],
            [qw( foo bar baz )],
        );

        return undef;
    }

    sub bar {
        return undef;
    }

    package MyPluginB;
    
    sub new { bless {}, shift }
    
    sub foo {
        my ( $self, @args ) = @_;
        
        main::isa_ok( $self, 'MyPluginB' );
        main::is_deeply(
            [ @args ],
            [qw( foo bar baz )],
        );

        return 'test!';
    }

    sub bar {
        return 'baz';
    }
}

my $api     = Tsukumo::API->new;
my $pluginA = MyPluginA->new;
my $pluginB = MyPluginB->new;

$api->register_hook(
    $pluginA,
    'test.hook' => $pluginA->can('foo'),
    'test.hook' => $pluginA->can('bar'),
);

$api->register_hook(
    $pluginB,
    'test.hook' => $pluginB->can('foo'),
    'test.hook' => $pluginB->can('bar'),
);

my $callback = sub {
    my ( $hook, $ret, $instance, @args ) = @_;

    is( $hook, 'test.hook' );
    like( ref $instance, qr{^MyPlugin(?:A|B)} );
    is_deeply(
        [ @args ],
        [qw( foo bar baz )],
    );

};

is_deeply(
    [ $api->run_hooks('test.hook' => [qw( foo bar baz )], callback => $callback ) ],
    [ undef, undef, 'test!', 'baz' ],
);

is( $api->run_hook_once('test.hook' => [qw( foo bar baz )]), 'test!' );

done_testing;
