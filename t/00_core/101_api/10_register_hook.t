#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::API;

{
    package MyPlugin;
    
    sub new { bless {}, shift }

    sub foo {}
}

my $api     = Tsukumo::API->new;
my $plugin  = MyPlugin->new;

$api->register_hook(
    $plugin,
    'foo.bar.baz' => $plugin->can('foo'),
);

is_deeply(
    $api->hooks,
    {
        'foo.bar.baz' => [
            { instance => $plugin, action => $plugin->can('foo') },
        ],
    },
);

done_testing;
