#!perl

use strict;
use warnings;

use Test::More;
use Tsukumo::Cache::Memory;
use Tsukumo::Cache::Cache;
use Tsukumo::Cache::CacheCache;
use Tsukumo::Cache::CHI;
use Tsukumo::Cache::Memcached;
use Tsukumo::Cache::FastMmap;

my @order = (
    {
        interface   => 'Memory',
    },
    {
        module      => 'Cache::Memory',
        interface   => 'Cache',
        args        => {},
        deref       => 1,
    },
    {
        module      => 'Cache::MemoryCache',
        interface   => 'CacheCache',
        args        => {},
        deref       => 0,
    },
    {
        module      => 'CHI',
        interface   => 'CHI',
        args        => { driver => 'Memory' },
        deref       => 1,
    },
    {
        module      => 'Cache::FastMmap',
        interface   => 'FastMmap',
        args        => {},
        deref       => 1,
    },
        sub {
        for my $module ( qw( Cache::Memcached::Fast Test::TCP ) ) {
            local $@;
            eval "use ${module}";
            if ( $@ ) {
                diag("Skip interface (Memcached) tests: ${module} is not installed");
                return;
            }
        }

        my $bin = $ENV{'TEST_MEMCACHED_BIN'};
        if ( ! $bin || ! -x $bin ) {
            diag("Skip interface (Memcached) tests: Set TEST_MEMCACHED_BIN environment variable to run this interface tests.");
            return;
        }

        my $port    = empty_port();
        my $memd    = Cache::Memcached::Fast->new( servers => [ { address => "localhost:${port}" } ] );
        my $cache   = Tsukumo::Cache::Memcached->new( cache => $memd );
        test_tcp(
            client  => sub { run_tests( $cache )    },
            server  => sub { exec $bin, '-p', $port },
            port    => $port,
        );
    },

);

for my $config ( @order ) {
    if ( ref $config eq 'HASH' ) {
        if ( exists $config->{'module'} ) {
            my $module      = delete $config->{'module'}    or die "Cache module is not specified";
            my $interface   = delete $config->{'interface'} or die "Interface is not specified.";
            my $args        = delete $config->{'args'} || {};
            my $deref       = !! $config->{'deref'};
            local $@;
            if ( eval "require ${module}; 1" ) {
                my $instance    = $module->new( ( $deref ) ? %{ $args } : $args );
                my $cache       = "Tsukumo::Cache::${interface}"->new( cache => $instance );
                run_tests( $cache );
            }
            else {
                diag("Skip interface (${interface}) test: ${module} is not installed.");
            }
        }
        else {
            my $interface   = delete $config->{'interface'} or die "Interface is not specified.";
            my $cache       = "Tsukumo::Cache::${interface}"->new;
            run_tests( $cache );
        }

    }
    elsif ( ref $config eq 'CODE' ) {
        $config->();
    }
}

done_testing;

sub run_tests {
    my ( $cache ) = @_;

    $cache->set( foo => 'bar' );

    is( $cache->get('foo'), 'bar' );

    $cache->set_multi( [ bar => 'baz' ], [ baz => 'foo' ] );

    is_deeply(
        $cache->get_multi(qw( foo bar baz )),
        {
            foo => 'bar',
            bar => 'baz',
            baz => 'foo',
        },
    );

    $cache->remove_multi(qw( bar));

    ok( ! $cache->get('bar') );

    $cache->clear();

    ok( ! $cache->get('foo') );
    ok( ! $cache->get('baz') );

}