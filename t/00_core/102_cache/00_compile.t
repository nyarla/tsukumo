#!perl

use strict;
use warnings;

use Test::More;

BEGIN {
    use_ok('Tsukumo::Cache');
    use_ok('Tsukumo::Cache::Cache');
    use_ok('Tsukumo::Cache::CacheCache');
    use_ok('Tsukumo::Cache::Memcached');
    use_ok('Tsukumo::Cache::FastMmap');
    use_ok('Tsukumo::Cache::CHI');
};

done_testing;
