package t::Util;

use strict;
use warnings;

use File::Spec;
use FindBin ();

use base qw( Exporter );

our ( $basedir, $examples );
our @EXPORT_OK = qw( $basedir $examples );

{
    my @path = File::Spec->splitdir( $FindBin::Bin );
    while ( my $dir = pop @path ) {
        if ( $dir eq 't' ) {
            $basedir    = File::Spec->catfile( @path );
            $examples   = File::Spec->catfile( @path, 't', 'examples' );
        }
    }
}

1;
